#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = begin;
	while (i != end)
	{
		if (is_prime(i))
		{
			primes.push_back(i);
		}

		i++;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	std::clock_t start;
	double duration;

	vector<int> primes;

	start = std::clock();

	thread t1(getPrimes, begin, end, ref(primes));
	t1.join();

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

	printVector(primes);
	cout << duration << " seconds" << endl;

	return primes;
}

bool is_prime(int num)
{
	int i = (int)sqrt(num) + 1;
	while (i != 1)
	{
		if (num % i == 0) return false;
		i--;
	}
	return true;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int i = 0;
	for (i = begin; i <= end; i++)
	{
		if (is_prime(i))
		{
			file << i << endl;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread> _threads;
	ofstream file;
	file.open(filePath);
	int diff = (end - begin) / N;
	int _end = begin + diff;
	int _begin = begin;
	const clock_t begin_time = clock();
	for (int i = 0; i < N; i++)
	{
		_threads.push_back(thread(writePrimesToFile, _begin, _end, ref(file)));
		_begin = _end;
		_end += diff;

	}
	for (int i = 0; i < N; i++)
	{
		_threads[i].join();
	}
	cout << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds" << endl;
	file.close();
}
